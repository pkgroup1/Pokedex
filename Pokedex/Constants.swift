//
//  Constants.swift
//  Pokedex
//
//  Created by praveen velanati on 2/17/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import Foundation

let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

typealias DownloadComplete = () -> ()

